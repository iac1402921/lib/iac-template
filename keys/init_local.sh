#!/usr/bin/env bash

######
# Блок настройки Openstack для подключения к облаку
##################

# MCS-специфичные настройки
export OS_AUTH_URL="https://cloud.ap-msk.com:5000/v3/"
export OS_PROJECT_NAME="playground" # ID проекта-песочницы
export OS_REGION_NAME="RZN-01"
export OS_USER_DOMAIN_NAME="Default"

export OS_PROJECT_DOMAIN_NAME="Default"
export OS_TENANT_NAME="playground"
 
# Убираем старые переменные
# unset OS_TENANT_ID
# unset OS_TENANT_NAME
# unset OS_PROJECT_DOMAIN_ID

# Если логин и пароль ещё не заданы в переменных окружения, запрашиваем их ввод у пользователя
if [[ -z $OS_USERNAME ]] || [[ -z $OS_PASSWORD ]]; then
	 
	echo "Please enter your OpenStack Username for project $OS_PROJECT_ID: "
	read -sr OS_USERNAME_INPUT
	export OS_USERNAME=$OS_USERNAME_INPUT

	echo "Please enter your OpenStack Password for project $OS_PROJECT_ID as user $OS_USERNAME: "
	read -sr OS_PASSWORD_INPUT
	export OS_PASSWORD=$OS_PASSWORD_INPUT

fi
if [[ -z $TF_VAR_dns_username ]] || [[ -z $TF_VAR_dns_password ]]; then
	 
	echo "Please enter your DNS Username: "
	read -sr DNS_USERNAME_INPUT
	export TF_VAR_dns_username=$DNS_USERNAME_INPUT

	echo "Please enter your DNS Password for user $TF_VAR_DNS_USERNAME: "
	read -sr DNS_PASSWORD_INPUT
	export TF_VAR_dns_password=$DNS_PASSWORD_INPUT

fi  


######
# Блок DNS-провайдера в части аутентификации
##################
# Задаём положение файла с конфигурацией kerberos для правильной работы механизма GSS-TSIG при работе с Microsoft DNS Server
export KRB5_CONFIG=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/.." &> /dev/null && pwd )/terraform/config/krb5_config
# echo $TF_VAR_DNS_PASSWORD >> $( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/.." &> /dev/null && pwd )/terraform/config/dns_p.key
# export DNS_P_FILE=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/.." &> /dev/null && pwd )/terraform/config/dns_p.key
# kinit --password-file=$DNS_P_FILE $TF_VAR_DNS_USERNAME
#printf "%b" "addent -password -p $TF_VAR_DNS_USERNAME@AP.COM -k 1 -e aes256-cts-hmac-sha1-96\n$TF_VAR_DNS_PASSWORD\nwrite_kt $TF_VAR_DNS_USERNAME.keytab" | ktutil
#printf "%b" "read_kt $TF_VAR_DNS_USERNAME.keytab\nlist" | ktutil
