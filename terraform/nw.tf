#-----------
## Create ext-network
#-----------
resource "openstack_networking_network_v2" "ext-network" {
  name           = "ext-network"
  admin_state_up = "true"
  external       = "true"
  segments {
    network_type     = "flat"
    physical_network = "physnet1"
  }
}

#-----------
## Create ext-subnet
#-----------
resource "openstack_networking_subnet_v2" "ext-subnet" {
  name            = "ext-subnet"
  network_id      = openstack_networking_network_v2.ext-network.id
  cidr            = "192.168.2.0/23"
  gateway_ip      = "192.168.2.1"
  dns_nameservers = ["192.168.2.254", "192.168.5.254"]
  enable_dhcp     = "false"
  allocation_pool {
    start = "192.168.3.80"
    end   = "192.168.3.120"
  }
}

#-----------
## Create external-router
#-----------
resource "openstack_networking_router_v2" "external-router" {
  name                = "external-router"
  admin_state_up      = "true"
  enable_snat         = "false"
  external_network_id = openstack_networking_network_v2.ext-network.id
}

#-----------
## Create int-network
#-----------
resource "openstack_networking_network_v2" "int-network" {
  name           = "int-network"
  admin_state_up = "true"
}

#-----------
## Create int-subnet-1
#-----------
resource "openstack_networking_subnet_v2" "int-subnet-1" {
  network_id = openstack_networking_network_v2.int-network.id
  name       = "int-subnet-1"
  cidr       = "10.10.1.0/24"
  gateway_ip = "10.10.1.1"
}

#-----------
## Create int-interface
#-----------
resource "openstack_networking_router_interface_v2" "int-interface" {
  router_id = openstack_networking_router_v2.external-router.id
  subnet_id = openstack_networking_subnet_v2.int-subnet-1.id
}
