# Больше примеров создания групп фаервола можно увидеть в ридми к модулю
# https://github.com/realscorp/tf-openstack-vkcs-secgroup

# module "i_int_example" {
#     source = "git@github.com:realscorp/tf-openstack-vkcs-secgroup.git"
#     name = "i_int_example"
#     description = "Group to access some service"
#     rules = [{
#                 direction = "ingress"
#                 protocol = "tcp"
#                 ports = ["1234", "900-1000"]
#                 remote_ips = var.i_int_example_ip_list
#                 }
#             ]
# }
module "i_sg_1" {
  source      = "git::https://gitlab.ap.com/iac/lib/tf-openstack-vkcs-secgroup.git"
  name        = "i_sg_1"
  description = "Group to access vm"
  rules = [{
    direction = "ingress"
    protocol  = "tcp"
    ports     = ["443", "22"]
    remote_ips = {
      "External LAN-RZN" = "192.168.2.0/23"
      "External LAN-MSK" = "192.168.4.0/23"
      "Internal LAN"     = "10.10.1.0/24"
    }
    },
    {
      direction = "ingress"
      protocol  = "udp"
      ports     = ["22"]
      remote_ips = {
        "External LAN-RZN" = "192.168.2.0/23"
        "External LAN-MSK" = "192.168.4.0/23"
        "Internal LAN"     = "10.10.1.0/24"
      }
    },
    {
      direction = "ingress"
      protocol  = "icmp"
      ports     = ["0"]
      remote_ips = {
        "External LAN-RZN" = "192.168.2.0/23"
        "External LAN-MSK" = "192.168.4.0/23"
        "Internal LAN"     = "10.10.1.0/24"
      }
  }]
}

module "o_sg_1" {
  source      = "git::https://gitlab.ap.com/iac/lib/tf-openstack-vkcs-secgroup.git"
  name        = "o_sg_1"
  description = "Group to access vm"
  rules = [{
    direction = "egress"
    protocol  = "tcp"
    ports     = ["443"]
    remote_ips = {
      "External LAN-RZN" = "192.168.2.0/23"
      "External LAN-MSK" = "192.168.4.0/23"
      "Internal LAN"     = "10.10.1.0/24"
    }
    },
    {
      direction = "egress"
      protocol  = "icmp"
      ports     = ["0"]
      remote_ips = {
        "External LAN-RZN" = "192.168.2.0/23"
        "External LAN-MSK" = "192.168.4.0/23"
        "Internal LAN"     = "10.10.1.0/24"
      }
  }]
}
