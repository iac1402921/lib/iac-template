terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }
  }
}

# Провайдер полностью настраивается через переменные окружения OS_
provider "openstack" {
}

provider "dns" {
  update {
    server = "ad.ap.com"
    gssapi {
      realm    = "AP.COM"
      username = var.dns_username
      password = var.dns_password
    }
  }
}

#-----------
## Create image cirros 061
#-----------

resource "openstack_images_image_v2" "cirros_061" {
  name             = "cirros-0.6.1"
  image_source_url = "https://download.cirros-cloud.net/0.6.1/cirros-0.6.1-x86_64-disk.img"
  container_format = "bare"
  disk_format      = "qcow2"

  properties = {
    os_version          = "0.6.1"
    hw_qemu_guest_agent = "yes"
    os_distro           = "Others"
    usage_type          = "common"
    os_admin_user       = "root"
  }
}

#-----------
## Create flavor standard
#-----------

resource "openstack_compute_flavor_v2" "standard-2-2-6" {
  description = "Standard flavor for all VM"
  name        = "standard-2-2-6"
  vcpus       = "2"
  ram         = "2048"
  disk        = "3"
  swap        = "0"
  ephemeral   = "3"
  is_public   = "true"

  extra_specs = {
    ":architecture"             = "x86_architecture",
    ":category"                 = "compute_optimized",
    "quota:disk_total_iops_sec" = "2000",
    "hw:numa_nodes"             = "1",
    "hw:mem_page_size"          = "any"
  }
}

resource "openstack_blockstorage_volume_v3" "vol-vm01" {
  name        = "vol-vm01"
  volume_type = "__DEFAULT__"
  size        = "3"
  image_id    = openstack_images_image_v2.cirros_061.id

  depends_on = [
    openstack_images_image_v2.cirros_061
  ]
}

#-----------
## Create Instance vm01-cirros
#-----------

resource "openstack_compute_instance_v2" "vm01-cirros" {
  name            = "vm01-cirros"
  flavor_id       = openstack_compute_flavor_v2.standard-2-2-6.id
  key_pair        = openstack_compute_keypair_v2.terraform.name
  security_groups = ["i_sg_1", "o_sg_1"]

  block_device {
    uuid                  = openstack_blockstorage_volume_v3.vol-vm01.id
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }
  metadata = {
    env = "dev"
  }

  depends_on = [
    openstack_blockstorage_volume_v3.vol-vm01,
    openstack_networking_subnet_v2.int-subnet-1,
    openstack_compute_keypair_v2.terraform
  ]

  network {
    name = "int-network"
  }
}
