resource "dns_a_record_set" "cirros_dns" {
  zone = var.dns_zone
  name = "cirros"
  addresses = [
    "192.168.7.11"
  ]
  ttl = 600
}
